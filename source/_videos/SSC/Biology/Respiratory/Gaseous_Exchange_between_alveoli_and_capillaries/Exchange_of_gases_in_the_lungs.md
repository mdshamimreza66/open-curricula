---
title: Exchange of gases in the lungs
youtube_id: WaLswydbVkM
tags:
    - SSC
    - BIOLOGY
    - Respiration
    - Animation
categories:
    - Gaseous Exchange
---
