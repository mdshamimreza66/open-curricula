---
title: Carriage of oxygen (Haemoglobin) 
youtube_id: bcpX20xXDEc
tags:
    - SSC
    - BIOLOGY
    - Respiration
categories:
    - Transportation of gases
---
