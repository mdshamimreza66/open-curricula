---
title: Transport of Respiratory Gases by Blood 
youtube_id: lteiBXSgSkM
tags:
    - SSC
    - BIOLOGY
    - Respiration
categories:
    - Transportation of gases
---
