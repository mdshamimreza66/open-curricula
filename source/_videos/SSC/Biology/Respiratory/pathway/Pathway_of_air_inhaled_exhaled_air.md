---
title: Pathway of air and inhaled vs. exhaled air
youtube_id: BUMfm-sWL7Y
tags:
    - SSC
    - BIOLOGY
    - Respiration
categories:
    - air_pathway
---
